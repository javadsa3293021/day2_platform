import java.util.*;
class Solution {
            public int[] twoSum(int[] nums, int target){
                    int arr[] = new int[2];
                    for(int i=0; i<nums.length; i++){
                            for(int j=i+1; j<nums.length; j++){

                                    if(nums[i] + nums[j] == target){

                                            arr[0] = i;
                                            arr[1] = j;
                                    }
                            }
                    }
                    return arr;
                }
}
class main{

        public static void main(String[] args){

                System.out.println("Enter the size of array");
                Scanner sc = new Scanner(System.in);
                int size = sc.nextInt();

                int A[] = new int[size];

                System.out.println("Enter the elements in the array");
                for(int i=0; i<size; i++){

                        A[i] = sc.nextInt();
                }
                System.out.println("Enter the Target");
                int target = sc.nextInt();

                Solution obj = new Solution();
                int B[] = obj.twoSum(A,target);
                System.out.println("Solution");
                for(int i = 0; i<B.length; i++){

                        System.out.println(B[i]);
                }
        }
}
