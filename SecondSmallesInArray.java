class Compute 
{
    public long[] minAnd2ndMin(long a[], long n)  
    {
        long[] _final={-1,-1};long fs=Long.MAX_VALUE;long ss=Long.MAX_VALUE;long[] ak={-1};
        for (int i=0;i<a.length;i++){
            if (fs>a[i]) {
                fs=a[i];
            }
            _final[0]=fs;
        }
        for (int i=0;i<a.length;i++){
            if (ss>a[i]&&a[i]!=fs){
                ss=a[i];
            }
            _final[1]=ss;
        }
        if (ss == Long.MAX_VALUE) {
            return ak;
        }
        else
            return _final;

    }
}
class Client{

	public static void main(String[] args){
	
		long[] a = new long[]{2, 4, 3, 5, 6};
		long n = a.length;

		Compute obj = new Compute();
		long[] b = obj.minAnd2ndMin(a,n);

		for(int i=0;i<b.length;i++){
		
			System.out.println(b[i]);
		}

	}
}
